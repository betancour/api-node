require('dotenv').config();
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');

const app = express();
const port = 3000;
const users = require('./routes/users')

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on('connected', () => {
  console.log('Connected to MongoDB');
});
mongoose.connection.on('Error', (err) => {
  console.log('MongoDB Error on ' + err)
});

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport');

app.use('/users', users);
app.get('/', (req, res) => {
  res.send('invalid EndPoint')
})

app.listen(port, () => {
  console.log('Server Started on Port ' + port);
});
