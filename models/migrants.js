/* eslint-disable no-multi-assign */
/* eslint-disable no-unused-vars */
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const MigrantSchema = mongoose.Schema(
  {
    person: {
      names: String,
      lastNames: String,
      age: Number,
      gender: String,
      maritalStatus: [],
      childrensNumbers: Number,
      childrensAges: [],
      childrensCountry: String,
      language: String,
      othersLanguage: [],
      driveLicence: Boolean,
      driveLicenceType: String,
    },
    migration: {
      countryOfBirth: String,
      timeInUy: Number,
      traveledBy: String,
      cameWith: String,
      uyTransitStatus: Boolean,
      traveledWithChildrens: Boolean,
      cameFromAnotherCountry: Boolean,
      howLongAnotherCountry: Number,
      whyChooseUY: String,
      kindOfMigrationStat: String,
      documentKind: String,
      documentID: Number,
      wealthID: Boolean,
      previdenceID: Boolean,
    },
    health: {
      healthSystem: Boolean,
      healthSystemName: String,
      accesibility: Boolean,
      accesibilityName: String,
      illness: String,
      medicalSupply: String,
      medicalReference: String,
    },
    work: {
      actuallyWork: Boolean,
      profession: String,
      boxRegistered: String,
      timeOnQueue: Number,
      timeStillOnQueue: Number,
      curriculumMade: Boolean,
      workAssistance: Boolean,
      curriculumByVolunteer: Boolean,
      webSitesRegistered: [],
    },
    contact: {
      phone: Boolean,
      phoneNumber: String,
      hasEmail: Boolean,
      email: String,
      hasSocialMedia: Boolean,
      socialMedia: [],
    },
    relationship: {
      mediaTargeted: [],
      mediaVisit: String,
      reasonToVisit: String,
      recomendedByOther: String,
      otherAssistance: String,
    },
    education: {
      educationLevel: String,
      profession: String,
      studyOnUY: Boolean,
      wantStudyOnUy: Boolean,
      documentsStudy: Boolean,
      educationAid: Boolean,
      formerCapacity: Boolean,
      educationCapacityArea: [],
    },
    living: {
      ownLiving: String,
      kindOfLiving: String,
      address: String,
      basicServicesList: [],
    },
  },
);

const Migrant = module.exports = mongoose.model('Migrant', MigrantSchema);
