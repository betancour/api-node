const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const User = require('../models/users');
const config = require('../config/database');

module.exports = function (passport) {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, (jwt_payloads, done) => {
    User.getUserById(jwt_payloads._id, (err, user) => {
      if (err) {
        return done(err, false);
      }
      if (user) {
        return (null, user);
      }
      return done(null, false);
    });
  }));
};
